def prefilter_items(data_train, n_popular=150, n_unpopular=1000, departments=None,\
                    min_value=np.inf, max_value=0):
    # Оставим только 5000 самых популярных товаров
    popularity = data_train.groupby('item_id')['quantity'].sum().reset_index()
    popularity.rename(columns={'quantity': 'n_sold'}, inplace=True)
    top_5000 = popularity.sort_values('n_sold', ascending=False).head(5000).item_id.tolist()
    #добавим, чтобы не потерять юзеров
    data_train.loc[~data_train['item_id'].isin(top_5000), 'item_id'] = 999999 
    

    # Уберем самые популярные 
    without_popular = top_5000[n_popular:]
    data_train.loc[~data_train['item_id'].isin(without_popular), 'item_id'] = 999999 
    
    # Уберем самые непопулряные 
    n = 5000 - n_popular - n_unpopular
    without_unpopular = without_popular[:n]
    data_train.loc[~data_train['item_id'].isin(without_unpopular), 'item_id'] = 999999 
    
    # Уберем товары, которые не продавались за последние 12 месяцев
    data_train.loc[data_train['day'] <= 3651, 'item_id'] = 999999
    
    # Уберем не интересные для рекоммендаций категории (department)
    try:
        data_train.loc[data_train['department'].isin(departments), 'item_id'] = 999999 
    except:
        pass
    # Уберем слишком дешевые товары (на них не заработаем). 1 покупка из рассылок стоит 60 руб. 
    data_train.loc[data_train['sales_value'] <= min_value, 'item_id'] = 999999
    # Уберем слишком дорогие товары
    data_train.loc[data_train['sales_value'] >= max_value, 'item_id'] = 999999
    # ...
    
    return data_train
    

def postfilter_items():
    pass

def get_ids_to_ids(user_item_matrix):
    userids = user_item_matrix.index.values
    itemids = user_item_matrix.columns.values

    matrix_userids = np.arange(len(userids))
    matrix_itemids = np.arange(len(itemids))
    
    global id_to_itemid, id_to_userid, itemid_to_id, userid_to_id
    
    id_to_itemid = dict(zip(matrix_itemids, itemids))
    id_to_userid = dict(zip(matrix_userids, userids))

    itemid_to_id = dict(zip(itemids, matrix_itemids))
    userid_to_id = dict(zip(userids, matrix_userids))

    
def get_similar_items_recommendation(item, model, N=5):
    """Рекомендуем товары, похожие на топ-N купленных юзером товаров"""
    
    res = [id_to_itemid[rec[0]] for rec in 
                    model.recommend(itemid=itemid_to_id[item], 
                                    user_items=csr_matrix(user_item_matrix).tocsr(),
                                    N=N, 
                                    filter_already_liked_items=False, 
                                    filter_items=None,  # !!! 999999 
                                    recalculate_user=True)]
    
    return res

def get_similar_users_recommendation(user, model, N=5):
    """Рекомендуем топ-N товаров, среди купленных похожими юзерами"""
    
    res = [id_to_itemid[rec[0]] for rec in 
                    model.recommend(userid=userid_to_id[user], 
                                    user_items=csr_matrix(user_item_matrix).tocsr(),
                                    N=N, 
                                    filter_already_liked_items=False, 
                                    filter_items=None,  # !!! 999999 
                                    recalculate_user=True)]

    return res

